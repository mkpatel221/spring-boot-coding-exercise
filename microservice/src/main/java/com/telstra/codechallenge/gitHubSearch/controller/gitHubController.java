package com.telstra.codechallenge.gitHubSearch.controller;


import com.telstra.codechallenge.gitHubSearch.DTO.GitHubUserResponse;
import com.telstra.codechallenge.gitHubSearch.service.GitHubService;
import com.telstra.codechallenge.gitHubSearch.model.GitHubUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.GitInfoContributor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class gitHubController {

    Logger logger = LoggerFactory.getLogger(GitInfoContributor.class);

    @Autowired
    private GitHubService gitHubService;

    @RequestMapping(path = "/gitHub/zeroFollowerUser", method = RequestMethod.GET)
    public ResponseEntity<GitHubUserResponse> getGitHubUsers(@RequestParam(value = "per_page", defaultValue = "10") String per_page){
        logger.info("Inside gitHubController GET: /gitHub/zeroFollowerUser");

        ResponseEntity responseEntity = ResponseEntity.ok(gitHubService.getUsers(per_page));

        logger.info("ResponseEntity is: "+responseEntity.toString());

        return responseEntity;

    }
}
